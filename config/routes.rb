# frozen_string_literal: true

Rails.application.routes.draw do
  devise_for :users, controllers: {
    sessions: 'api/v1/users/sessions',
    passwords: 'api/v1/users/passwords'
    # registrations: 'api/v1/registrations'
  }
  namespace 'api' do
    namespace 'v1' do
      # post '/login', to: 'users#login'
      get '/users', to: 'users#show'
      resources :employees, except: %i[index show]
      get '/employee', to: 'employees#show'
      # post '/employee/:expertise_id', to: 'employees#create'
      post '/employees/assign_expertise', to: 'expertises_employees#assign_expertise'
      get '/employees_expertises', to: 'expertises_employees#show'
      get '/attendances', to: 'attendances#index'
      post '/checkin', to: 'attendances#checkin'
      post '/checkout', to: 'attendances#checkout'
      get '/info', to: 'attendances#info'

      resources :expertises

      resources :client_companies

      resources :projects, except: %i[create]
      post '/projects/:client_company_id', to: 'projects#create'

      resources :project_teams, except: %i[create]
      post '/project_teams/:project_id', to: 'project_teams#create'
    end
  end
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
