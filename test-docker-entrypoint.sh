#!/bin/sh

set -e

echo "ENVIRONMENT: $RAILS_ENV"

bundle exec ${@}