# create_table "employees", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
#   t.string "employeeCode"
#   t.string "fullname"
#   t.string "idCardNo"
#   t.string "taxCardNo"
#   t.datetime "joinAt"
#   t.string "email"
#   t.string "phone"
#   t.datetime "bod"
#   t.string "pob"
#   t.string "idCardAddress"
#   t.string "currentAddress"
#   t.string "emergencyContactName"
#   t.string "emergencyContact"
#   t.datetime "created_at", precision: 6, null: false
#   t.datetime "updated_at", precision: 6, null: false
# end



require 'rails_helper'

RSpec.describe Employee, type: :model do
  context 'validation test' do
    let!(:params){{
      employeeCode: 'BTS02',
      fullname: 'Bayu Suryo Aji',
      idCardNo: '12345678901234567',
      taxCardNo: '5432156789065',
      joinAt: '2021-06-15',
      email: 'bayus@gmail.com',
      phone: '6281999839873',
      bod: '1992-11-16',
      pob: 'Wonogiri',
      idCardAddress: 'Wonogiri',
      currentAddress: 'Sukajadi',
      emergencyContactName: 'Lucia',
      emergencyContact: '6282524325245'
    }}

    it 'ensure email presence' do
      employee = Employee.new({ employeeCode: 'asdas' }).save
      expect(employee).to eq(false)
    end

    it 'ensure employeeCode presence' do
      employee = Employee.new(email: 'asdasd@mail.com').save
      expect(employee).to eq(false)
    end

    it 'ensure new employee was saved' do
      employee = Employee.new(params).save
      expect(employee).to eq(true)
    end
  end
end
