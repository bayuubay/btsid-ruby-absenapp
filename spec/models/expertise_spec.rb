require 'rails_helper'

RSpec.describe Expertise, type: :model do
  context 'validation test' do
    it 'expertise name is presence' do
      expertise = create(:expertise, name: '')
      expect(expertise.valid?).to eq(true)
    end
  end
end
