FactoryBot.define do
  factory :project_team do
    employee_id {}
    project_id {}
    position { Faker::Lorem.characters(number: 6) }
    start_date { Faker::Date }
    end_date { Faker::Date }
  end
end
