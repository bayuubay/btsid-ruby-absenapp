require 'faker'
FactoryBot.define do
  factory :employee do
    employeeCode { Faker::Lorem.characters(number: 5) }
    fullname { Faker::Lorem.characters(number: 5) }
    idCardNo { Faker::Lorem.characters(number: 5) }
    taxCardNo { Faker::Lorem.characters(number: 5) }
    joinAt { Faker::Date }
    email { Faker::Internet.email }
    phone { Faker::PhoneNumber.cell_phone }
    bod { Faker::Date }
    pob { Faker::Lorem.characters(number: 6) }
    idCardAddress { Faker::Lorem.characters(number: 6) }
    currentAddress { Faker::Lorem.characters(number: 6) }
    emergencyContactName { Faker::Lorem.characters(number: 5) }
    emergencyContact { Faker::PhoneNumber.cell_phone }
  end
end
