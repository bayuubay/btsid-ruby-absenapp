FactoryBot.define do
  factory :attendance do
    employee_id {}
    checkin { Faker::Date }
    checkout { Faker::Date }
    checkinLat { Faker::Address.latitude }
    checkinLng { Faker::Address.longitude }
    checkoutLat { Faker::Address.latitude }
    checkoutLng { Faker::Address.longitude }
    selfie_url { Faker::Lorem.characters(number: 10) }
  end
end
