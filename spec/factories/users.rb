FactoryBot.define do
  factory :user do
    # id { Faker::Internet.uuid }
    username { Faker::Internet.username }
    email { Faker::Internet.email }
    password { Faker::Internet.password(min_length: 10) }
    role { 0 }
    # employee_id { Faker::Internet.uuid }
  end
end
