FactoryBot.define do
  factory :client_company do
    company_name { Faker::Lorem.characters(number: 10) }
  end
end
