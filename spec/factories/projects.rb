FactoryBot.define do
  factory :project do
    name { Faker::Lorem.characters(number: 10) }
    start_date { Faker::Date }
    end_date { Faker::Date }
    actual_start_date { Faker::Date }
    actual_end_date { Faker::Date }
    client_company_id { create(:client_company).id }
  end
end
