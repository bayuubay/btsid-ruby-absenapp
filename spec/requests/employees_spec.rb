require 'rails_helper'

RSpec.describe 'Employee API', type: :request do
  before(:each) do
    @employee = build(:employee)
  end
  let!(:params) do
    {
      employeeCode: Faker::Lorem.characters(number: 5),
      fullname: Faker::Lorem.characters(number: 5),
      idCardNo: Faker::Lorem.characters(number: 5),
      taxCardNo: Faker::Lorem.characters(number: 5),
      joinAt: Faker::Date,
      email: Faker::Internet.email,
      phone: Faker::PhoneNumber.cell_phone,
      bod: Faker::Date,
      pob: Faker::Lorem.characters(number: 6),
      idCardAddress: Faker::Lorem.characters(number: 6),
      currentAddress: Faker::Lorem.characters(number: 6),
      emergencyContactName: Faker::Lorem.characters(number: 5),
      emergencyContact: Faker::PhoneNumber.cell_phone
    }
  end
  context 'POST /create' do
    it 'create new employee' do
      count_employee = Employee.count
      post '/api/v1/employees', params: { employee: params }
      expect(response).to have_http_status(201)
      expect(Employee.count).to be > count_employee
    end
  end
  context 'PUT /update' do
    before do
      login_user
    end
    it 'update employee data' do
      @employee.save
      put "/api/v1/employees/#{@employee.id}", params: { employee: params }
      expect(response).to have_http_status(201)
    end
  end
  context 'DELETE /destroy' do
    before do
      login_user
    end
    it 'delete employee data' do
      @employee.save
      count_employee = Employee.count
      delete "/api/v1/employees/#{@employee.id}"
      expect(response).to have_http_status(200)
      expect(Employee.count).to be < count_employee
    end
  end
end
