require 'rails_helper'

RSpec.describe 'ExpertiseEmployees', type: :request do
  let(:name) { Faker::Lorem.characters(number: 5) }
  before(:each) do
    @expertise = build(:expertise, name: name).save
    # @employee = build(:employee).save
    login_user
  end
  context 'POST /Assign Expertise' do

    it 'assign expertise to employee' do
      post '/api/v1/employees/assign_expertise', params: { expertises: name }
      expect(response).to have_http_status(201)
    end
  end
end
