require 'rails_helper'

RSpec.describe 'Projects', type: :request do
  before(:each) do
    @client_company = build(:client_company)
    @project = build(:project)
    # @project.client_company_id = @client_company.id
    login_user
  end
  let!(:params) do
    {
      project: {
        name: Faker::Lorem.characters(number: 10),
        start_date: Faker::Date,
        end_date: Faker::Date,
        actual_start_date: Faker::Date,
        actual_end_date: Faker::Date
      }
    }
  end
  context 'POST /create' do
    it 'create new projects' do
      @client_company.save
      post "/api/v1/projects/#{@client_company.id}", params: params
      expect(response).to have_http_status(201)
    end
  end

  context 'PUT /update' do
    it 'update project data' do
      @project.save
      id = @project.id
      put "/api/v1/projects/#{id}", params: params
      expect(response).to have_http_status(201)
    end
  end

  context 'DELETE /destroy' do
    it 'delete project entry' do
      @project.save
      # expect(@project.id).to be_true
      delete "/api/v1/projects/#{@project.id}"
      expect(response).to have_http_status(200)
    end
  end
end
