require 'rails_helper'
require 'faker'
RSpec.describe 'Attendances', type: :request do
  before(:each) do
    login_user
  end
  context 'POST api/v1/checkin' do
    it 'create checkin data' do
      post '/api/v1/checkin', params: { checkin: {
        checkinLat: Faker::Address.latitude,
        checkinLng: Faker::Address.longitude
      } }
      expect(response).to have_http_status(200)
    end
  end
  context 'POST api/v1/checkout' do
    before do
      @attendance = create(:attendance, employee_id: @user.employee_id, checkinLat: Faker::Address.latitude, checkinLng: Faker::Address.longitude)
    end
    it 'create checkout data' do
      post '/api/v1/checkout', params: { attendance: {
        checkoutLat: Faker::Address.latitude,
        checkoutLng: Faker::Address.longitude
      } }
      expect(response).to have_http_status(200)
    end
  end
  context 'GET api/v1/info' do
    before do
      @attendance = create(:attendance, employee_id: @user.employee_id)
    end
    it 'show user attendance data' do
      get '/api/v1/info'
      expect(response).to have_http_status(200)
    end
  end
end
