require 'rails_helper'

RSpec.describe "ProjectTeams", type: :request do
  before(:each) do
    @project = create(:project)
    login_user
    @project_team = create(:project_team, employee_id:@user.employee_id, project_id:@project.id)
  end
  let(:position_name){ Faker::Lorem.characters(number: 6) }
  let(:params) {
    {
      position: position_name,
      start_date: Faker::Date,
      end_date: Faker::Date
    }
  }
  context "GET /index" do
    it 'show all project teams' do
      get '/api/v1/project_teams'
      expect(response).to have_http_status(200)
    end
  end

  context "POST /create" do
    it 'show all project teams' do
      post "/api/v1/project_teams/#{@project.id}", params: params
      expect(response).to have_http_status(201)
      expect(response.body).to include(position_name)
    end
  end

  context 'PUT /update' do
    it 'update project team data' do
      put "/api/v1/project_teams/#{@project_team.id}", params: params
      expect(response).to have_http_status(201)
    end
  end
  context 'DELETE /destroy' do
    it 'delete project team entry' do
      delete "/api/v1/project_teams/#{@project_team.id}"
      expect(response).to have_http_status(200)
    end
  end
end
