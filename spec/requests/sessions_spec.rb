require 'rails_helper'

RSpec.describe 'Sessions', type: :request do
  let(:fake_username) { Faker::Internet.username }
  let(:fake_password) { Faker::Internet.password }

  before(:each) do
    @employee = create(:employee)
    @user = create(:user, employee_id: @employee.id, username: fake_username, password: fake_password )
  end
  it 'signs user in' do
    post '/users/sign_in.json', params: { user: { username: fake_username, password: fake_password } }
    expect(response).to have_http_status(200)
  end
end
