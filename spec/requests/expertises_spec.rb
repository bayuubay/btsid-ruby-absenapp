require 'rails_helper'
# require_relative '../support/devise'
RSpec.describe "Expertises", type: :request do
  before(:each) do
    @expertise = build(:expertise)
    login_user
  end
  let!(:params) { Faker::Lorem.characters(number: 5) }
  describe 'GET /index' do
    it 'show all expertises' do
      get '/api/v1/expertises'
      expect(response).to have_http_status(200)
      expect(response.body.length).to be > 0
    end
  end

  describe 'POST /create' do
    it 'create new expertise' do
      post '/api/v1/expertises', params: { expertise: { name: params } }
      expect(response).to have_http_status(201)
      expect(response.body).to include(params)
    end
  end
  describe 'PUT /update' do
    it 'update exepertise data' do
      @expertise.save
      put "/api/v1/expertises/#{@expertise.id}", params: { expertise: { name: params } }
      expect(response).to have_http_status(201)
    end
  end
  describe 'DELETE /destroy' do
    it 'delete expertise entry' do
      @expertise.save
      delete "/api/v1/expertises/#{@expertise.id}"
      expect(response).to have_http_status(200)
    end
  end
end
