require 'rails_helper'
require 'faker'

RSpec.describe "ClientCompanies", type: :request do
  # need to skip authentification process
  before(:each) do
    @comp_name = Faker::Lorem.characters(number: 5)
    @client_company = build(:client_company, company_name: @comp_name)
    login_user
  end
  # let!(:comp_name){ Faker::Lorem.characters(5) }
  let!(:params){ 'test' }
  context 'POST /create' do
    it 'create new a client company' do
      post '/api/v1/client_companies', params: { client_company: { company_name: params } }
      expect(response).to have_http_status(201)
      expect(response.body).to include(params)
    end
  end
  context 'GET /index' do
    it 'show all client_companies' do
      get '/api/v1/client_companies'
      expect(response).to have_http_status(200)
      expect(response.body).to include(params)
      expect(response.body.length).to be > 0
    end
  end
  context 'GET /show' do
    it 'show particular client company' do
      @client_company.save
      get "/api/v1/client_companies/#{@client_company.id}"
      expect(response).to have_http_status(200)
      expect(response.body).to include(@comp_name)
    end
  end
  context 'PUT /update' do
    it 'update client company data' do
      @client_company.save
      put "/api/v1/client_companies/#{@client_company.id}", params: { client_company: { company_name: params } }
      expect(response).to have_http_status(201)
    end
  end
  context 'DELETE /destroy' do
    it 'delete client company entry' do
      @client_company.save
      delete "/api/v1/client_companies/#{@client_company.id}"
      expect(response).to have_http_status(200)
    end
  end
end
