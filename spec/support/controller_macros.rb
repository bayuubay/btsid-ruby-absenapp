module ControllerMacros
  def login_user
    employee = create(:employee)
    @user = create(:user, employee_id: employee.id)
    sign_in @user
  end
  
  def logout_user
    sign_out @user
  end
end
