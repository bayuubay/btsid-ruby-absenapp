class AddImageUrlToAttendance < ActiveRecord::Migration[6.1]
  def change
    add_column :attendances, :selfie_url, :string
  end
end
