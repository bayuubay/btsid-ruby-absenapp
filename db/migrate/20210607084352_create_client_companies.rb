# frozen_string_literal: true

class CreateClientCompanies < ActiveRecord::Migration[6.1]
  def change
    create_table :client_companies, id: :uuid do |t|
      t.string :companyName

      t.timestamps
    end
  end
end
