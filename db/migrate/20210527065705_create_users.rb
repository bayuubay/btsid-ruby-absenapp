# frozen_string_literal: true

# User table migration
class CreateUsers < ActiveRecord::Migration[6.1]
  def change
    enable_extension 'pgcrypto'
    create_table :users, id: :uuid do |t|
      t.string :username
      t.string :email
      t.string :password_digest
      t.references :employee, null: false, foreign_key: true, type: :uuid
      t.numeric :role

      t.timestamps
    end
  end
end
