# frozen_string_literal: true

class CreateProjects < ActiveRecord::Migration[6.1]
  def change
    create_table :projects, id: :uuid do |t|
      t.references :client_company, null: false, foreign_key: true, type: :uuid
      t.string :name
      t.datetime :start_date
      t.datetime :end_date
      t.datetime :actual_start_date
      t.datetime :actual_end_date

      t.timestamps
    end
  end
end
