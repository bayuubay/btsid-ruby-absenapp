# frozen_string_literal: true

class CreateExpertisesEmployees < ActiveRecord::Migration[6.1]
  def change
    enable_extension 'pgcrypto'
    create_table :expertises_employees, id: :uuid do |t|
      t.references :employee, null: false, foreign_key: true, type: :uuid
      t.references :expertise, null: false, foreign_key: true, type: :uuid

      t.timestamps
    end
  end
end
