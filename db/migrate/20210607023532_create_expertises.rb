# frozen_string_literal: true

class CreateExpertises < ActiveRecord::Migration[6.1]
  def change
    enable_extension 'pgcrypto'
    create_table :expertises, id: :uuid do |t|
      t.string :name

      t.timestamps
    end
  end
end
