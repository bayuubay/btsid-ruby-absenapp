# frozen_string_literal: true

class CreateProjectTeams < ActiveRecord::Migration[6.1]
  def change
    create_table :project_teams, id: :uuid do |t|
      t.references :employee, null: true, foreign_key: true, type: :uuid
      t.references :project, null: false, foreign_key: true, type: :uuid
      t.string :position
      t.datetime :start_date
      t.datetime :end_date

      t.timestamps
    end
  end
end
