# frozen_string_literal: true

# AttendanceMailer mailer
class AttendanceMailer < ApplicationMailer
  default from: ENV['GMAIL_USERNAME']
  # before_action :authorized
  def check_in_email(attendance)
    @attendance = attendance
    @employee = params[:employee]
    mail(to: @employee.email, subject: "Checkin: #{@employee.fullname} at #{@attendance.checkin}")
  end

  def check_out_email(attendance)
    @attendance = attendance
    @employee = params[:employee]
    mail(to: @employee.email, subject: "Checkout: #{@employee.fullname} at #{@attendance.checkout}")
  end
end
