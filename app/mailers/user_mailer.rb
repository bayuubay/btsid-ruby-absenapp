# frozen_string_literal: true

# AttendanceMailer mailer
class UserMailer < ApplicationMailer
  default from: ENV['GMAIL_USERNAME']
  
  def registrations_confirmation(user)
    @user = user
    @employee = params[:employee]
    mail(to: @employee.email, subject: "Registration: #{@employee.fullname}")
  end

  # def check_out_email(attendance)
  #   @attendance = attendance
  #   @employee = params[:employee]
  #   mail(to: @employee.email, subject: "Checkout: #{@employee.fullname}")
  # end
end
