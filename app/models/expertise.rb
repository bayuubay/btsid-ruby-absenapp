# frozen_string_literal: true

class Expertise < ApplicationRecord
  has_many :expertises_employees
  has_many :employees, through: :expertises_employees
  validates :name, uniqueness: { case_sensitive: false, message: 'similar expertise already exist' }
end
