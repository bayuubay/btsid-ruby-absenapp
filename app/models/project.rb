# frozen_string_literal: true

class Project < ApplicationRecord
  belongs_to :client_company
  has_many :project_teams
  has_many :employees, through: :project_teams
end
