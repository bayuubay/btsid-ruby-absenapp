# frozen_string_literal: true

# User Model
class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable,
         :jwt_authenticatable,
         jwt_revocation_strategy: JwtDenylist
  # has_secure_password
  belongs_to :employee

  validates :username, presence: true
  validates :password, presence: true
  def jwt_payload
    {
      employee_id: employee_id,
      id: id,
      email: email
    }
  end
end
