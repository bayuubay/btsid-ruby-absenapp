# frozen_string_literal: true

# Employee Model
class Employee < ApplicationRecord
  has_many :attendances, dependent: :destroy
  has_one :user, dependent: :destroy
  has_many :expertises_employees
  has_many :expertise, through: :expertises_employees
  has_many :project_teams
  has_many :projects, through: :project_teams
  validates :email, presence: true, uniqueness: true, format: { with: URI::MailTo::EMAIL_REGEXP }
  validates :employeeCode, presence: true, uniqueness: true
end
