# frozen_string_literal: true

# Attendance Model
class Attendance < ApplicationRecord
  # before_update :checkout_date
  has_one_attached :selfie
  belongs_to :employee

  def checkin_date(selfie)
    self.checkin = DateTime.now.to_s(:db)
    self.selfie_url = selfie
    save
  end

  def checkout_date(checkout_params)
    self.checkout = DateTime.now.to_s(:db)
    update(checkout_params)
  end

  def attendance_info(current_employee)
    {
      fullname: current_employee.fullname,
      nip: current_employee.employeeCode,
      email: current_employee.email,
      checkin: checkin,
      checkin_selfie: selfie_url,
      checkout: checkout
    }
  end
end
