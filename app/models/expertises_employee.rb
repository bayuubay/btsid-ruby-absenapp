# frozen_string_literal: true

class ExpertisesEmployee < ApplicationRecord
  belongs_to :employee
  belongs_to :expertise
end
