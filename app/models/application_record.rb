# frozen_string_literal: true

# Application Models
class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
end
