# frozen_string_literal: true

class ProjectTeam < ApplicationRecord
  belongs_to :employee
  belongs_to :project
end
