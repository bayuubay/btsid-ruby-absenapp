# frozen_string_literal: true

class ClientCompany < ApplicationRecord
  has_many :projects, dependent: :destroy
  validates :company_name, presence: true, uniqueness:
  {
    case_sensitive: false,
    message: 'this company name alredy existed'
  }
end
