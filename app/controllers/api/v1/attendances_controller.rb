# frozen_string_literal: true

module Api
  module V1
    # attendance controller
    class AttendancesController < ApplicationController
      before_action :current_employee, :current_attendance, only: %i[info]
      def index
        render json: { message: current_employee }.to_json
      end

      def info
        attendance_data = current_attendance.attendance_info(current_employee)
        if attendance_data
          render json: {
            data: attendance_data, code: 200, message: 'success'
          }
        else
          render json: {
            data: attendance_data.errors, code: 404, message: 'not found'
          }, status: :not_found
        end
      end

      def checkin
        @attendance = current_employee.attendances.build(checkin_params)
        if @attendance.checkin_date(@attendance.selfie.url)
          # puts "RESPONSE: #{@attendance.checkin}"
          AttendanceMailer.with(employee: current_employee).check_in_email(@attendance).deliver_now
          render json: { data: @attendance, code: 200, message: 'success' }
        else
          render json: { code: 400, message: 'something went wrong' }
        end
      end

      def checkout
        @attendance = Attendance.where(employee_id: current_user.employee_id, checkout: nil).first
        if @attendance&.checkout_date(checkout_params)
          AttendanceMailer.with(employee: current_employee).check_out_email(@attendance).deliver_now
          render json: { data: @attendance, code: 200, message: 'success' }
        else
          render json: { code: 422, message: 'no checkin data or you have already checked out' }, status: 422
        end
      end

      private

      def checkin_params
        params.permit(:checkinLat, :checkinLng, :selfie, :employee_id)
      end

      def checkout_params
        params.require(:attendance).permit(:checkoutLat, :checkoutLng)
      end

      def current_employee
        Employee.find(current_user.employee_id)
      end

      def current_attendance
        Attendance.find_by(employee_id: current_employee.id)
      end

    end
  end
end
