# frozen_string_literal: true

module Api
  module V1
    # Expertise Controller
    class ExpertisesController < ApplicationController
      # skip_before_action :authenticate_user!
      before_action :expertise_params, only: %i[create]
      before_action :current_expertise, only: %i[show update destroy]
      def index
        # render json: { message: 'success test routes' }
        @expertises = Expertise.all
        if @expertises
          render json: { data: @expertises, code: 200, message: 'success' }, status: :ok
        else
          render json: { code: 404, message: @expertises.errors }, status: :not_found
        end
      end

      def show
        if current_expertise
          render json: { data: current_expertise, code: 200, message: 'success' }, status: :ok
        else
          render json: { code: 404, message: current_expertise.errors }, status: :not_found
        end
      end

      def create
        @expertise = Expertise.create(expertise_params)
        if @expertise.save
          render json: { data: @expertise, code: 201, message: 'success' }, status: :created
        else
          render json: { code: 422, message: @expertise.errors }, status: :unprocessable_entity
        end
      end

      def update
        if current_expertise&.update(expertise_params)
          render json: { data: current_expertise, code: 201, message: 'success' }, status: :created
        else
          render json: { code: 422, message: current_expertise.errors }, status: :unprocessable_entity
        end
      end

      def destroy
        if current_expertise&.destroy
          render json: { code: 200, message: 'success' }, status: :ok
        else
          render json: { code: 422, message: current_expertise.errors }, status: :unprocessable_entity
        end
      end

      private

      def expertise_params
        params.require(:expertise).permit(:name)
      end

      def current_expertise
        Expertise.find(params[:id])
      end
    end
  end
end
