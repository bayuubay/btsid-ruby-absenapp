# frozen_string_literal: true

module Api
  module V1
    # Employee Controller
    class EmployeesController < ApplicationController
      skip_before_action :authenticate_user!, only: %i[create]
      before_action :current_employee, only: %i[update destroy]
      after_action :register_user, only: [:create]
      def show
        @employee = Employee.find(current_user.employee_id)
        return unless @employee

        render json: { data: @employee, code: 200, message: 'success' }, status: :ok
      end

      def create
        @employee = Employee.new(employee_params)
        if @employee.save
          # register_user(@employee)
          render json: { data: @employee, code: 201, message: 'success' }.to_json, status: :created
        else
          render json: { data: @employee.errors, code: 400, message: 'failed' }
        end
      end

      def update
        current_employee.update(employee_params)
        if current_employee.valid?
          render json: { code: 201, message: 'success' }, status: :created
        else
          render json: { code: 400, message: 'failed' }
        end
      end

      def destroy
        if current_employee.destroy
          render json: { code: 200, message: 'success delete data employee' }
        else
          render json: { code: 200, message: 'failed' }
        end
      end

      private

      def employee_params
        params.require(:employee).permit(
          :employeeCode, :fullname, :idCardNo, :taxCardNo, :joinAt, :email, :phone, :bod,
          :pob, :idCardAddress, :currentAddress, :emergencyContactName, :emergencyContact
        )
      end

      def current_employee
        Employee.find(params[:id])
      end

      def register_user
        @user = User.create(register_params(@employee))
        return unless @user.save

        UserMailer.with(employee: @employee).registrations_confirmation(@user).deliver_now
      end

      def register_params(employee)
        {
          username: employee.email.split('@').first,
          email: employee.email,
          password: employee.email.split('@').first,
          employee_id: employee.id,
          role: 0
        }
      end
    end
  end
end
