# frozen_string_literal: true

module Api
  module V1
    # ClientCompanies Controller
    class ClientCompaniesController < ApplicationController
      # skip_before_action :authenticate_user!
      before_action :client_comp_params, only: %i[create]
      def index
        @companies = ClientCompany.all
        render json: { data: @companies, code: 200, message: 'success' }.to_json, status: :ok
      end

      def show
        render json: { data: current_company, code: 200, message: 'success' }.to_json, status: :ok
      end

      def create
        @company = ClientCompany.create(client_comp_params)
        if @company.save
          render json: { data: @company, code: 201, message: 'success' }.to_json, status: :created
        else
          render json: { data: @company.errors, code: 400, message: 'failed' }
        end
      end

      def update
        if current_company&.update(client_comp_params)
          render json: { code: 201, message: 'success' }.to_json, status: :created
        else
          render json: { data: current_company.errors, code: 400, message: 'failed' }
        end
      end

      def destroy
        if current_company&.destroy
          render json: { code: 200, message: 'success' }.to_json, status: :ok
        else
          render json: { data: current_company.errors, code: 400, message: 'failed' }
        end
      end

      private

      def client_comp_params
        params.require(:client_company).permit(:company_name)
      end

      def current_company
        ClientCompany.find(params[:id])
      end
    end
  end
end
