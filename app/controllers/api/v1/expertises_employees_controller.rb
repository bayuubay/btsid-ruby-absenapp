# frozen_string_literal: true

module Api
  module V1
    # Expertise_Employee Controller
    class ExpertisesEmployeesController < ApplicationController
      before_action :current_expertise, only: [:assign_expertise]
      def assign_expertise
        @exp_employee = ExpertisesEmployee.create({ expertise_id: @expertise.id, employee_id: current_employee.id })
        if @exp_employee.save
          render json: { data: @exp_employee, code: 201, message: 'success' }, status: :created
        else
          render json: { code: 422, message: @exp_employee.errors }
        end
      end

      def show
        render json:
        {
          data: {
            employee_name: current_employee.fullname,
            expertises: current_expertise_employee
          }
        }
      end

      private

      def current_employee
        Employee.find(current_user.employee_id)
      end

      def current_expertise
        @expertise = Expertise.find_by_name(params[:expertises])
        return render json: { message: "expertise item of '#{params[:expertises]}' did not existed" } unless @expertise
      end

      def current_expertise_employee
        ExpertisesEmployee.where(employee_id: current_employee.id).joins(:expertise).select('expertises.name')
      end
    end
  end
end
