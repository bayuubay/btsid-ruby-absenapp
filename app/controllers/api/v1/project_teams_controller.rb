# frozen_string_literal: true

module Api
  module V1
    # Project Teams Controller
    class ProjectTeamsController < ApplicationController
      def index
        @project_teams = ProjectTeam.all
        render json: { data: @project_teams, code: 200, message: 'success' }, status: :ok
      end

      def show
        render json: { data: current_project_teams, code: 200, message: 'success' }, status: :ok
      end

      def create
        @project_teams = current_employee.project_teams.build(project_teams_params)
        if @project_teams.save
          render json: { data: @project_teams, code: 201, message: 'success' }, status: :created
        else
          render json: { code: 422, message: @project_teams.errors }, status: :unprocessable_entity
        end
      end

      def update
        if current_project_teams&.update(project_teams_params)
          render json: { data: current_project_teams, code: 201, message: 'success' }, status: :created
        else
          render json: { code: 422, message: current_project_teams.errors }, status: :unprocessable_entity
        end
      end

      def destroy
        if current_project_teams&.destroy
          render json: { code: 200, message: 'success' }, status: :ok
        else
          render json: { code: 422, message: current_project_teams.errors }, status: :unprocessable_entity
        end
      end

      private

      def project_teams_params
        params.permit(:position, :start_date, :end_date, :project_id)
      end

      def current_employee
        Employee.find(current_user.employee_id)
      end

      def current_project_teams
        ProjectTeam.find(params[:id])
      end
    end
  end
end
