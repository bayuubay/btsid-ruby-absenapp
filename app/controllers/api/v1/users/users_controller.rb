# frozen_string_literal: true

module Api
  module V1
    module Users
      # User Controller
      class UsersController < ApplicationController
        # before_action :asd
        def show
          render json: { user: 'hello' }
        end
        # def asd(user)
        #   User.find(user.id)
        # end
      end
    end
  end
end
