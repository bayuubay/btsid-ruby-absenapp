# frozen_string_literal: true

module Api
  module V1
    module Users
    # SessionsController
      class SessionsController < Devise::SessionsController
        respond_to :json
        # after_action :fetch_header, only: [:create]

        private

        def respond_with(_resource, _opts = {})
          if resource.id?
            # puts "'RESOURCE: #{resource.id}'"
            render json: { code: 200, message: 'You are logged in.' }, status: :ok
          else
            render json: { code: 401, message: 'You are not authorized or wrong credentials' }, status: :unauthorized
          end
        end

        def respond_to_on_destroy
          log_out_success && return if current_user

          log_out_failure
        end

        def log_out_success
          render json: { message: 'You are logged out.' }, status: :ok
        end

        def log_out_failure
          render json: { message: 'nothing happened.' }, status: :unauthorized
        end

        # def fetch_header
        #   token = response.headers
        #   puts "TOKEN: #{token}"
        # end
      end
    end
  end
end
