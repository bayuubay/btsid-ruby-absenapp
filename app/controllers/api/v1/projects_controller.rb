# frozen_string_literal: true

module Api
  module V1
    # Projects Controller
    class ProjectsController < ApplicationController
    # before_action :current_client_company, only: [:create]
      before_action :current_project, only: %i[update show]
      def index
        @projects = Project.all
        render json: { data: @projects, code: 200, message: 'success' }
      end

      def create
        @project = current_client_company.projects.build(project_params)
        if @project.save
          render json: { data: @project, code: 201, message: 'success' }, status: :created
        else
          render json: { code: 422, message: @project.errors }, status: :unprocessable_entity
        end
      end

      def update
        if current_project&.update(project_params)
          render json: { data: current_project, code: 201, message: 'success' }, status: :created
        else
          render json: { code: 422, message: @project.errors }, status: :unprocessable_entity
        end
      end

      def destroy
        if current_project&.destroy
          render json: { code: 200, message: 'success' }, status: :ok
        else
          render json: { code: 404, message: @project.errors.message }, status: :unprocessable_entity
        end
      end

      def show
        render json: { data: current_project, code: 200, message: 'success' }, status: :ok
      end

      private

      def project_params
        params.require(:project).permit(:name, :start_date, :actual_start_date,
                                        :end_date, :actual_end_date, :client_company_id)
      end

      def current_client_company
        ClientCompany.find(params[:client_company_id])
      end

      def current_project
        Project.find(params[:id])
      end
    end
  end
end
