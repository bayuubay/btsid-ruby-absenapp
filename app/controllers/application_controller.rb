# frozen_string_literal: true

# Application Controller
class ApplicationController < ActionController::API
  before_action :authenticate_user!
  # :logged_in_user
  # before_action :authorized, except: :index
  # def encode_token(payload)
  #   JWT.encode(payload, ENV['SECRET_KEY'])
  # end

  # def auth_header
  #   request.headers['Authorization']
  # end

  # def decoded_token
  #   return unless auth_header

  #   token = auth_header.split(' ')[1]
  #   begin
  #     JWT.decode(token, ENV['SECRET_KEY'], true, algorithm: 'HS256')
  #   rescue JWT::DecodeError
  #     nil
  #   end
  # end

  # def logged_in_user
  #   return unless decoded_token

  #   user_id = decoded_token[0]['id']
  #   @user = User.where('id = ?', user_id).select('id, employee_id, email').first
  #   # @user = decoded_token[0]
  # end

  # def logged_id?
  #   !!logged_in_user
  # end

  # def authorized
  #   render json: { message: 'Please log in' }, status: :unauthorized unless logged_id?
  # end
end
