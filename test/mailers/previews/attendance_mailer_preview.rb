# frozen_string_literal: true

# Preview all emails at http://localhost:3000/rails/mailers/attendance_mailer
class AttendanceMailerPreview < ActionMailer::Preview
  OrderMailer.sample_email
end
