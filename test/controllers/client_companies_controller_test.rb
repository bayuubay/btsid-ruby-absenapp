# frozen_string_literal: true

require 'test_helper'

class ClientCompaniesControllerTest < ActionDispatch::IntegrationTest
  test 'should get index' do
    get client_companies_index_url
    assert_response :success
  end

  test 'should get show' do
    get client_companies_show_url
    assert_response :success
  end

  test 'should get create' do
    get client_companies_create_url
    assert_response :success
  end

  test 'should get update' do
    get client_companies_update_url
    assert_response :success
  end

  test 'should get destroy' do
    get client_companies_destroy_url
    assert_response :success
  end
end
