# frozen_string_literal: true

require 'test_helper'

class ExpertisesControllerTest < ActionDispatch::IntegrationTest
  test 'should get index' do
    get expertises_index_url
    assert_response :success
  end

  test 'should get create' do
    get expertises_create_url
    assert_response :success
  end

  test 'should get update' do
    get expertises_update_url
    assert_response :success
  end

  test 'should get destroy' do
    get expertises_destroy_url
    assert_response :success
  end
end
